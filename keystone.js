// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone'),
	ejs = require('ejs'),
	partials = require('express-partials'),
	pkg = require('./package.json');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({

	//'mongo': 'mongodb://94.202.111.26:27017/rwatgg', //Dubai laptop
	// 'mongo': process.env.MONGO_URI || "mongodb://localhost:27017",
	'mongo': "mongodb://localhost:27017",

	'name': 'rwatgg',
	'brand': 'rwatgg',

	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',

	'view engine': 'ejs',
	'custom engine': ejs.renderFile,

	// use the layout.ejs as default
	'view options': {
		layout: true
	},

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',

	'cookie secret': 'BegZJqe!k5mQ<*~r*|8>MRV64G*{[hAWa&W&BeS6*tob-:ZST+B$^=Hi+Pf/[N}]'

});

keystone.set('s3 config', {
    bucket: 'rwatgg',
    key: 'AKIAJOKD5FC3RV5EU2NA',
    secret: 'gSQMJeAgpoXjV19TVoAt1czpr9jH3b0avfYtTmTA'
});


// make it possible for us to use the layout and partials
// in layout we have <% body %>, this get's replaced by the individual views
keystone.app.use(partials());

// Load your project's Models

keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,

	// google analytics id ->
	googleAnalyticsID: pkg.googleAnalyticsID,

	// get the scripts from the package file (dependency reasons)
	// used in main layout.ejs file
	scripts: pkg.projectFiles.scripts,

	// get the styles from the package file (dependency reasons)
	// used in main layout.ejs file
	styles: pkg.projectFiles.styles
});


keystone.set('port', process.env.PORT || 1337);

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Setup common locals for your emails. The following are required by Keystone's
// default email templates, you may remove them if you're using your own.

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	'users': 'users',
    'pages': ['TextPages'],
    'social': ['SocialLinks'],
    'cases': ['cases', 'case-contents', 'case-types']
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();
