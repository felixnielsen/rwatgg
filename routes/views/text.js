var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;

	locals.section = "Contact"
	locals.title = "Contact us"

	if(req.xhr) {
        return res.json({
            data: locals.pageData
        });
    } else {
    	// view.query('cases', data);
        return view.render("partials/templates/text", {
        	data: locals.pageData   
        });
    };
	
};

// get the data from mongo
exports.get = module.exports.get = function(req, res, next) {
	var locals = res.locals;
	locals.pageData = {};

	keystone.list('TextPages')
        .model
        .where("name", "Contact")
        .populate('content')
        .populate('type')
        .exec(function(err, data) {
        	locals.pageData = data;
        	next();
        });
};
