var keystone = require('keystone');

exports.setMenu = function(req, res, next) {

	function setLocalsMenu(menu)
	{
		res.locals.menu = menu;
	};

	// check if the menu is in the cache
    if(Object.getOwnPropertyNames(cache.get("menu")).length > 0)
    {
    	// set locals menu
    	setLocalsMenu(cache.get("menu").menu);
    	
    	// continue in the app
    	next();

    	// prevent further logic
        return;
    }

	// generate menu, as it is not in the cache
	var cases,
		about,
		contact,
		locals = res.locals,
		find = locals.isAdmin ? {} : { published: true };

	var menu = {
		"cases": undefined,
		"text pages": undefined
	};

	var checkQuerie = function()
	{
		if(menu["cases"] !== undefined && menu["text pages"] !== undefined && menu["social"] !== undefined)
		{
			// set menu in the cache
		    cache.set("menu", menu);
		    
		    // set locals menu
		    setLocalsMenu(cache.get("menu").menu);

		    // continue in the app
			next();
		}
	};

	keystone.list('Cases')
		.model
		.find(find)
		.where("offline", "false")
		.populate('content')
		.populate('type')
		.exec(function(err, data) {
			menu["cases"] = data;

			// build types
			var _types = [];
			for (var i = 0; i < data.length; i++) {
				var _filters = [];
				for (var j = 0; j < data[i].type.length; j++) {
					_filters.push(data[i].type[j].key);
					if(_types.indexOf(data[i].type[j]) == -1)
					{
						_types.push(data[i].type[j]);
					}
				}

				// set filters to use in front-end
				data[i].filters = _filters.toString();
			};

			_types.sort(function(a, b)
			{
				// sort for visual perfection
				if(b.key.length > a.key.length)
					return 1
				else
					return 0
			})

			menu["filter-types"] = _types;

			checkQuerie();
	});

	keystone.list('TextPages').model.find().exec(function(err, data) {
		menu["text pages"] = data;
		checkQuerie();
	});

	keystone.list('SocialLinks').model.find().exec(function(err, data) {
		menu["social"] = data;
		checkQuerie();
	});

	return;
};
