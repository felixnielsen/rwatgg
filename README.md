	      ___           ___           ___       ___           ___     
	     /\  \         /\  \         /\__\     /\  \         |\__\    
	    /::\  \       /::\  \       /:/  /    /::\  \        |:|  |   
	   /:/\:\  \     /:/\:\  \     /:/  /    /:/\:\  \       |:|  |   
	  /::\~\:\  \   /::\~\:\  \   /:/  /    /::\~\:\  \      |:|__|__ 
	 /:/\:\ \:\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ ____/::::\__\
	 \/_|::\/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\/:/  / \::::/~~/~   
	    |:|::/  /   \:\ \:\__\    \:\  \        \::/  /   ~~|:|~~|    
	    |:|\/__/     \:\ \/__/     \:\  \       /:/  /      |:|  |    
	    |:|  |        \:\__\        \:\__\     /:/  /       |:|  |    
	     \|__|         \/__/         \/__/     \/__/         \|__|    


Keystone.js is used for CMS, keystone is running on Expreess.js->Node.js

For Keystone related issues; report or check out their forum: https://groups.google.com/forum/#!forum/keystonejs

**Setup keystone:**
Install dependencies:

	npm install

Following steps here: http://keystonejs.com/getting-started/

- - - -

**Start local mongodb instance:**
CLI: sudo mongod (starts from /data/db)
Check connection here: http://localhost:27017/, should show:

	It looks like you are trying to access MongoDB over HTTP on the native driver port.



**Start Keystone:**
CLI: node keystone
Access -> http://localhost:1337/

- - - -

**Routing:**
Keystone routing, define routes in **routes/views/index.js**
Front-end routing, add to **Router.coffee**

- - - -

**Front-end code:**
place in source. Coffee, styl, images etc.
Everything will get compiled to **/public**

- - - -

## gulp

Link coffeescript gulp to dependencies.

	sudo npm link coffee-script

**Tasks:**

	gulp build
	gulp watch

#### Startup issues?
	module.js:340
    	throw err;
          ^
	Error: Cannot find module 'coffee-script'

Forgot to link coffee-script node module -> run in command-line:

	sudo npm link coffee-script
