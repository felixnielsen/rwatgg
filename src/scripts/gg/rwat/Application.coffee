#      ___           ___           ___       ___           ___     
#     /\  \         /\  \         /\__\     /\  \         |\__\    
#    /::\  \       /::\  \       /:/  /    /::\  \        |:|  |   
#   /:/\:\  \     /:/\:\  \     /:/  /    /:/\:\  \       |:|  |   
#  /::\~\:\  \   /::\~\:\  \   /:/  /    /::\~\:\  \      |:|__|__ 
# /:/\:\ \:\__\ /:/\:\ \:\__\ /:/__/    /:/\:\ \:\__\ ____/::::\__\
# \/_|::\/:/  / \:\~\:\ \/__/ \:\  \    \/__\:\/:/  / \::::/~~/~   
#    |:|::/  /   \:\ \:\__\    \:\  \        \::/  /   ~~|:|~~|    
#    |:|\/__/     \:\ \/__/     \:\  \       /:/  /      |:|  |    
#    |:|  |        \:\__\        \:\__\     /:/  /       |:|  |    
#     \|__|         \/__/         \/__/     \/__/         \|__|    
#     ___
#   _/ ..\
#  ( \  0/___
#   \    \___)
#   /     \
#  /      _\
# `''''``       
# filename: application.coffee
# created: 06/06/2014
# author: felix nielsen <felix@flexmotion.com>
window.application =

	stageModel: null
	colors: null
	sounds: null
	pages: null

	siteMenu: null
	threeD: null
	footer: null
	intro: null

	initialize : ->
		@stageModel = new StageModel
			mediaqueries:
				[
						horizontalBreakPoint: 640
						eventName: "mobile"
					,
						horizontalBreakPoint: 768	
						eventName: "tablet"
					,
						horizontalBreakPoint: 100000
						eventName: "desktop"
				]

		obj =
			"*": DefaultPage
			"case": CasePage
			"text": CasePage

		@tracking = new GATracking()
		
		# set the page models View Types
		PageModel.setPageTypes(obj)

		@pages = new PagesCollection
			filterTypes: ["filter"]
			$context: $("#page-content")
			rootURL: "/"

		@colors = new ColorTheme()

		@router = new Router
			pages: @pages

		@intro = new IntroView
			el: $("#intro-context")[0]

		@intro.on "done", @onIntroFinished, @
		@intro.runTheShow()

		@threeD = new ThreeWorld
			cases: $("#header-list > a:not('.text'):not('.social')")

		@siteMenu = new SiteMenu
			el: $("#header-menu")[0]

	onIntroFinished: ->
		@scrollHandler = new ScrollHandler()

		@siteMenu.peakABoo()

		@footer = new SiteFooter
			el: $("#footer")[0]

		@sounds = new SoundController()

		@threeD.peakABoo()

		# start the whole thing
		@router.start()

$ ->
	relax.setLog window.location.href.indexOf("localhost") is -1
	if window.location.href.indexOf("localhost") isnt -1
		$("body").addClass "localhost"

	application.initialize()

	true
