class SiteMenuFilterButton extends Backbone.View

	events:
		"click": "onClick"

	initialize: ->
		@model.on "change:button-visible", @onButtonVisibilityChange, @
		@model.on "change:visible", @onVisibilityChange, @

		@onVisibilityChange()

	update: (isFilterMode) ->
		

	onVisibilityChange: ->
		span = @$el.find("span").eq(0)
		text = if @model.get("visible") then span.data("alt-label") else span.data("label")
		span.text(text)

	onButtonVisibilityChange: ->
		# onButtonVisibilityChange

	onClick: (event) ->
		if @model.get("button-visible")
			event.preventDefault()

			if Backbone.history.fragment.indexOf("filter") isnt -1
				window.application.router.navigate "/"
			else
				filters = @model.get("filters")
				if filters and filters.length > 0
					window.application.router.navigate "filter/"+filters.toString()
				else
					window.application.router.navigate "filter"
