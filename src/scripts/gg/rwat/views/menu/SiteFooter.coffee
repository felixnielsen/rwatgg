class SiteFooter extends Backbone.View

	events:
		"click a": "onItemClick"

	_impressumLabel: null
	_backShowing: false

	initialize: ->
		@_impressumLabel = @$el.find("a").text()
		window.application.pages.on "updated", @onPageUpdated, @

	onPageUpdated: (model, dto) ->
		url = dto.url

		isImpressumPage = url.indexOf("impressum") isnt -1
		if isImpressumPage
			@_backShowing = true
			@$el.find("a").text "back"
		else
			@_backShowing = false
			@$el.find("a").text @_impressumLabel

		if isImpressumPage or url is ""
			@$el.removeClass "hide"
		else
			@$el.addClass "hide"

	onItemClick: (event) ->
		event.preventDefault()

		href = if !@_backShowing then $(event.currentTarget).attr("href") else "/"
		window.application.router.navigate href
