class SiteMenu extends Backbone.View

	events:
		"click #filter-button": "onFilterClick"

	_y: 0
	_yTarget: 0
	_minMenuY: 0
	_topPadding: 0
	_hiddenPadding: 0
	_savedScrollTop: 0
	_topPaddingTarget: 0
	_backButtonVisible: false
	_initialPositionSet: false

	_swiper: null
	_filterModel: null

	_animatedIn: false
	_hidden: null
	_items: null
	_index: -1

	initialize: ->
		@_filterModel = new SiteMenuFilterModel()

		window.application.pages.on "updated", @onPageUpdated, @

		@_items = []
		i = 0
		for el in @$el.find("a")
			@_items.push new SiteMenuItem
				el: el
				index: i++
		if Modernizr.touch
			@_swiper = new relax.tools.DragAndSwipe $(window)
			@_swiper.addDrag relax.dom.bind(@, @onDrag)

		window.application.stageModel.bind StageModel.RESIZE, @resize, @

	peakABoo: ->

		if !Modernizr.touch
			window.application.stageModel.bind StageModel.SCROLL, @onScroll, @

		@setY window.application.stageModel.h

		@$el.css("opacity", 1)

		if Modernizr.touch
			@setYTarget(0)

	onPageUpdated: (model, dto) ->
		url = dto.url

		@_index = @$el.find("a[href='/"+url+"']").index()
		isFilterURL = url.indexOf("filter") isnt -1
		isImpressumURL = url.indexOf("impressum") isnt -1
		isHomePage = dto.type is "home" or dto.type is ""

		if url is "" or url is "/" or isFilterURL
			@setVisibility doShow = true
		else
			@setVisibility doShow = false

		if @$el.hasClass("impressum-showing") then @$el.removeClass "impressum-showing"

		if @_swiper
			if Modernizr.touch and !isHomePage
				@_swiper.removeEvents()
			else
				@_swiper.addEvents()

		# let us know if the back-button is visible, so we can check for it's visible position
		@_backButtonPosition = false

		if isFilterURL
			@$el.find("#filter-button").addClass "filter-mode"
			@$el.find("#header-list").addClass "filter-mode"
			filters = @_filterModel.get "filters"

			for item in @_items
				item.hasFilter(filters)
		else if isImpressumURL
			@$el.addClass "impressum-showing"
		else
			@$el.find("#filter-button").removeClass "filter-mode"
			@$el.find("#header-list").removeClass "filter-mode"
			@_backButtonPosition = @$el.find(".back-button").position().top if @$el.find(".back-button").length > 0

			for item in @_items
				item.hasFilter([])

	renderQueCall: ->
		t = @_yTarget - @_y
		p = @_topPaddingTarget - @_topPadding

		if Math.abs(Math.round(t)) > 0 or Math.abs(Math.round(p)) > 0
			# easing
			@_y += t * ScrollHandler.TIME_SCROLL_SPEED
			@_topPadding += p * ScrollHandler.TIME_SCROLL_SPEED
		else
			relax.tools.renderQue.remove @

		# add padding
		_y = @_y + @_topPadding
		
		if @_backButtonPosition isnt false
			backYWithY = _y + @_backButtonPosition

			# desktop, perfect! mobile.. semi, not sure the problem resides here...
			if backYWithY < 0
				_y += backYWithY * -1
			else if backYWithY > @_topPaddingTarget
				_y = @_topPaddingTarget - @_backButtonPosition

		@setY _y

	setY: (value) ->
		@$el.css
			"transform": "translate3d(0px, "+value+"px, 0px)"

	resize: (model) ->
		@_topPaddingTarget = model.h - 60 # 60 is a niice position, cuts the first menu element in half. You tease you.
		paddingOffset = model.h - @_topPaddingTarget

		height = @$el.find("#header-list").height()

		@_minMenuY = height * -1

		if !@_initialPositionSet
			# animate in ..
			@_topPadding = @_topPaddingTarget + (paddingOffset * 2)

		@renderQueCall()

		@_initialPositionSet = true

		@_swiper?.setBoundries
			# 600 = el margin-top
			minSwipeY: @_minMenuY
			maxSwipeY: 1

		for item in @_items
			item.resize()

		@setScrollHeight()

		if !relax.tools.renderQue.has @
			relax.tools.renderQue.add @

	setScrollHeight: ->
		if !@_hidden and !Modernizr.touch
			window.application.scrollHandler?.setHeight window.application.stageModel.h * 3

	onDrag: (obj) -> @setYTarget obj.y if !@_hidden

	onScroll: (obj) ->
		obj.event?.preventDefault()
		percent = window.application.scrollHandler.getPercent()
		@setYTarget percent * @_minMenuY

	setYTarget: (value) ->
		@_yTarget = value + @_hiddenPadding

		@_y = @_yTarget if !@_animatedIn

		@_animatedIn = true

		if !relax.tools.renderQue.has @
			relax.tools.renderQue.add @

	setVisibility: (show) ->
		if @_hidden isnt !show
			@_hidden = !show

			@$el.removeClass "hidden"
			@$el.addClass "hidden" if @_hidden

			@_filterModel.set "button-visible", show

			if !Modernizr.touch
				if @_hidden
					@_savedScrollTop = window.application.stageModel.scrolltop 
					@_hiddenPadding = @_yTarget
				else
					@setScrollHeight()
					window.application.scrollHandler.setY @_savedScrollTop

					@_hiddenPadding = 0
			else
				@_savedScrollTop = 0
				@_hiddenPadding = 0

			for item in @_items
				item.setSelected @_index, !@_hidden

		@setScrollHeight()













