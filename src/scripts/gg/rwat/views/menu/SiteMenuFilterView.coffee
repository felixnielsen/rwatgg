class SiteMenuFilterView extends Backbone.View

	events:
		"click li": "onListItemClick"

	initialize: ->
		@model.on "change:visible", @onVisibilityChange, @
		window.application.pages.on "updated", @onPageUpdated, @

	update: ->
		@$el.find("li").removeClass "checked"
		filters = @model.get "filters"
		for filter in filters
			@$el.find("li[data-value='"+filter+"']").addClass "checked"

	onListItemClick: (event) ->
		value = $(event.currentTarget).data("value")
		@model.toggleFilter(value)

	onVisibilityChange: ->
		if @model.get("visible")
			@$el.addClass "visible"
		else
			@$el.removeClass "visible"
