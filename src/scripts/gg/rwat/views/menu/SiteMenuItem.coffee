class SiteMenuItem extends Backbone.View

	events:
		"click": "onClick"

	@ANIMATION_TIME: 0.6

	_clickable: true
	_selected: false
	_index: -1
	_delay: 0

	_hoverBomb: null
	_defaultLabel: null
	_label: null
	_color: null

	_filters: null

	_sphere: null

	initialize: (options) ->
		@_index = options.index
		@_defaultLabel = @_label = @$el.find("li .label").html()

		@_hoverBomb = new HoverBomb @$el.find("li .button-animation-1-context")

		if @$el.data("color")
			@_color = Number("0x"+@$el.data("color").split("#")[1])
		else
			@_color = 0xffffff

		li = @$el.find("> li")
		if Modernizr.touch
			li.on "touchstart", relax.dom.bind(@, @onMouseOver)
			li.on "touchend", relax.dom.bind(@, @onMouseOut)
		else
			li.on "mouseover", relax.dom.bind(@, @onMouseOver)
			li.on "mouseout", relax.dom.bind(@, @onMouseOut)

		@$el.addClass "clickable"

		if @$el.data("filters")
			@_filters = @$el.data("filters").split(",")

		if @$el.hasClass("text")
			@_sphere = window.application.threeD.rwatggSphere
		else
			@_sphere = window.application.threeD.getMesh(@_index)

	setX: (x, animationTime = null) ->
		animationTime = SiteMenuItem.ANIMATION_TIME if animationTime is null

		@_x = x
		TweenMax.to @$el, animationTime,
			"transform": "translateX(" + @_x + "px)"
			delay: @_delay
			ease: "easeInOutExpo"

	hasFilter: (filters) ->
		if @_filters and filters.length > 0
			@$el.removeClass "filtered-out"
			filteredOut = false
			for filter in filters
				if @_filters.indexOf(filter) is -1
					filteredOut = true

			@$el.addClass "filtered-out" if filteredOut
		else
			@$el.removeClass "filtered-out"

	resize: (animateX = null) ->
		# mobile, offset a bit, so we can show the menu

		if window.application.stageModel._currentMQ is "mobile"
			xoffset = 38
		else if window.application.stageModel._currentMQ is "tablet"
			xoffset = 100
		else
			xoffset = 120

		x = if !@_clickable then (window.application.stageModel.w - xoffset) else 0

		if @_selected
			isTextElement = @$el.hasClass "text"

			if window.application.stageModel._currentMQ is "mobile"
				backbuttonOffset = if isTextElement then 25 else 60
			else if window.application.stageModel._currentMQ is "tablet"
				backbuttonOffset = if isTextElement then 20 else 80
			else
				backbuttonOffset = if isTextElement then 50 else 130

			@setX x - backbuttonOffset, animateX
		else
			@setX x, animateX

	setSelected: (focusIndex, clickable) ->
		@_clickable = clickable

		@$el.removeClass "clickable"
		@$el.addClass "clickable" if @_clickable

		@_selected = focusIndex is @_index
		if focusIndex isnt -1
			@_delay = Math.abs(focusIndex - @_index) * 0.01

		if @_selected
			@setText "back"
			@$el.addClass "clickable"
		else
			@_hoverBomb.out()
			@setText()

		@resize(true)

	setText: (label = null) ->
		label = if label is null then @_defaultLabel else label

		if label is "back"
			@$el.addClass "back-button" if !@$el.hasClass "back-button"
		else if @$el.hasClass "back-button"
			@$el.removeClass "back-button"

		if @_label isnt label
			@_label = label
			li = @$el.find("li")

			obj = 
				index: 1
				length: @_label.length

			t = SiteMenuItem.ANIMATION_TIME * 0.5
			TweenMax.to obj, t,
				index: obj.length
				delay: if @_selected then t else 0.1
				ease: "easeInOutCubic"
				onUpdate: =>
					_i = Math.round(obj.index)
					li.find(".label").html(@_label.substr(0, _i))

	onMouseOver: (event) ->
		if @_sphere and @_clickable
			@_sphere.electricute()

		if @$el.hasClass("clickable") and !@$el.hasClass("filtered-out")
			@_hoverBomb.over()

			isTextElement = @$el.hasClass "text"
			isSocialElement = @$el.hasClass "social"

			if !isSocialElement
				window.application.threeD.rwatggSphere.hiFromMenuItem @_color
	
	onMouseOut: (event) ->
		if @_sphere
			@_sphere.unelectricute()
		if @$el.hasClass("clickable") and !@$el.hasClass("filtered-out")
			@_hoverBomb.out()
			window.application.threeD.rwatggSphere.unhiFromMenuItem()

	onClick: (event) ->
		event.preventDefault() #prevent a tag from going NUTS!
		@_hoverBomb.out()

		if @_clickable
			href = $(event.currentTarget).attr("href")
			if $(event.currentTarget).find("> li[type=social]").length is 1
				window.application.Tracking.event("social", "click", href)
				window.open href
			else
				window.application.router.navigate href

				window.application.sounds.play("click")

				isTextElement = @$el.hasClass "text"
				if !isTextElement
					window.application.threeD.rwatggSphere.hiFromMenuItem @_color, true
		else
			window.application.router.navigate "/"
