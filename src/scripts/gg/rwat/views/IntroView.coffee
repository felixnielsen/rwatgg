class IntroView extends Backbone.View
	
	_mobileRestrictedPercent: 0
	_currentLogoStep: 0
	_followScroll: false
	_logoHeight: 0
	_logoWidth: 0
	_swiper: null #only touch
	_logo: null
	_text: null

	progress: 0

	@INTRO_PERCENTAGE_OFSCROLL: 0.4

	initialize: ->
		#TODO: Brug TweenLite/Max til animere intro procenten.
		#TODO: Fjern drag og scroll handlers, brug kun tween. 

		window.application.pages.on "updated", @onPageUpdated, @

		@_logo = @$el.find(".logo")
		@_text = @$el.find(".intro-text")

		@_logoWidth = if relax.browser.isMobile() then 150 else 280 #logo width
		@_logoHeight = Math.round(@_logoWidth * 0.74566474) #logo height
	
		@setPercent 0.0001
		setTimeout =>
			@setPercent 0.0001
		, 0

	runTheShow: ->
		TweenLite.to @, 1,
			progress: 0.318181818
			delay: 0.5
			onUpdate: =>
				@setPercent()
			onComplete: =>
				TweenLite.to @, 1,
					easing: "easeOutExpo"
					progress: 1
					delay: 1.5
					onUpdate: =>
						@setPercent()
					onComplete: =>
						@onRunningTheShow()

	onRunningTheShow: ->
		@trigger "done"

		@$el.addClass "done"

		if !Modernizr.touch
			window.application.stageModel.bind StageModel.SCROLL, @onScroll, @
		else
			@_swiper = new relax.tools.DragAndSwipe $(window)
			@_swiper.addDrag relax.dom.bind(@, @onDrag)
			@showHideRWATGGText true

	# bad practice... but we do it anyway
	onDrag: -> @showHideRWATGGText window.application.siteMenu._y > -10

	onScroll: (model) -> @showHideRWATGGText window.application.scrollHandler.getPercent() < 0.01

	onPageUpdated: (model, dto) ->
		isHomePage = dto.type is "home" or dto.type is ""

		if !isHomePage
			@showHideRWATGGText(false)

			if Modernizr.touch
				@_mobileRestrictedPercent = 1
				@setPercent @_mobileRestrictedPercent

		@_followScroll = isHomePage

		@onDrag() if isHomePage and Modernizr.touch

	showHideRWATGGText: (show) ->
		if (show and @_followScroll) and !@_text.hasClass("show")
			@_text.addClass "show"
		else if (!show or !@_followScroll) and @_text.hasClass("show")
			@_text.removeClass "show"

	setPercent: ->
		clearTimeout @_removeTextTimer

		#TODO: flyt text removeClass til scrollHandler
		if @progress > 0.85 and @progress < 1 and !@_text.hasClass("show")
			
		else if @progress < 0.85 and @_text.hasClass("show")
			@_text.removeClass "show"
		else if @progress is 1 and @_text.hasClass("show")
			@_removeTextTimer = setTimeout =>
				@_text.removeClass "show"
			, 500

		numSteps = 22 # check css file
		_step = numSteps * @progress

		@$el.attr "class", ""

		@_currentLogoStep = Math.round(numSteps * @progress)
		for i in [0...@_currentLogoStep] by 1
			@$el.addClass "step"+Math.max(i, 0)

		scaleSection = 0.4 # 0.4 - 1 what part of the intro progress do we place the logo
		scalePercent = 1 - (Math.max(0, @progress - scaleSection) / (1 - scaleSection))

		logoWidth = relax.math.lerp(scalePercent, @_logoWidth, @_logoWidth * 2)
		logoHeight = relax.math.lerp(scalePercent, @_logoHeight, @_logoHeight * 2)

		yoffset = (@progress * 20) # how close should the bar be to the top?
		y = logoHeight - yoffset + (scalePercent * (window.application.stageModel.h - logoHeight))
		y = y * 0.5 - logoHeight * 0.5
		x = window.application.stageModel.w * 0.5 - logoWidth * 0.5

		@_logo.css
			"width": logoWidth
			"height": logoHeight
			"transform": "translateY(" + y + "px)"
