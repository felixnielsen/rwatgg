class ScrollHandler
	el: null
	_h: 0
	_stageModel: null

	@TIME_SCROLL_SPEED: 0.2

	constructor: ->
		@el = $("#pseudo-scroll")

		@_stageModel = window.application.stageModel

		#TODO: we should just set the height depending on the page/view??

		@_stageModel.bind StageModel.RESIZE, @resize, @

	setHeight: (value) ->
		@_h = value * 1.1 #TODO: hvorfor 1.1?
		@el.height @_h

	setY: (value) ->
		$(window).scrollTop value

	resize: (model) ->

	getPercent: ->
		overflowHeight = @_h - @_stageModel.h
		
		p = 1 - ((overflowHeight - $(window).scrollTop()) / overflowHeight)

		p = 0 if isNaN(p)

		p
