class DefaultPage extends Backbone.View

	tagName: "div"
	className: "page"

	render: (extraParameters = null) ->

		if @model.get("first-time-set")
			#page-content is already in the dom, rendered by the Node.js app.
			@$el.append($("#page-content > *"))
		else
			obj = _.extend @model.toJSON()
			# obj.addGlobalValuesHere = null

			if extraParameters
				for k, v of extraParameters
					obj[k] = v

			try
				template = _.template $( "script.template-" + @model.get("type") ).html()
			catch e
				#template does not exist ..
				template = _.template $( "script.template-default" ).html()

			data = @model.get("data")

			tmpl = template
				data: data

			@$el.append _.unescape(tmpl)

		window.application.scrollHandler.setY 0

		window.application.stageModel.bind StageModel.RESIZE, @resize, @, true
		window.application.stageModel.bind StageModel.SCROLL, @scroll, @, true

		@

	update: (dto) ->
		# update, ex when filtering on a page via the router
		log "page update requested", dto

	resize: (model) ->
		#log "resize", model

	scroll: (model) ->
		#log "scroll", model

	animateIn: ->
		log "view: animate in"

	animateOut: (newModel) ->
		log "view: animate out and remove when done"
		@dealoc()

	dealoc: ->
		window.application.stageModel.unbindAll @

		@unbind()
		@remove()
