class CaseGalleryImage extends relax.displayObject.Image

	init: ->
		_.extend @, Backbone.Events

	onImageLoaded: ->
		super()
		if @data.naturalWidth > 600 and @data.naturalHeight > 300
			$(@data).click relax.dom.bind @, @onClicky
			$(@data).addClass "clickable"

	onClicky: -> @trigger "click", $(@data).attr("src")

	position: (x, y, w, h) ->
		$(@data).css
			"left": x
			"top": y
			"width": w
			"height": h

	dealoc: ->
		$(@data).unbind() if @data
		super()

class CaseMobileGalleryImage extends CaseGalleryImage
	onImageLoaded: ->
		super()
		$(@data).click relax.dom.bind @, @onClicky

	onClicky: ->
		if !$(@data).hasClass("selected")
			$(@data).parent().find("img, iframe").removeClass "selected"
			$(@data).addClass "selected"
		else
			$(@data).removeClass "selected"

	dealoc: ->
		$(@data).unbind() if @data
		super()
