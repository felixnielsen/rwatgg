# gallery of each section, gallery contains imags and iframes (video)
class CaseGallery extends Backbone.View
	_elements: null
	_loaded: false

	initialize: ->
		# vimeo, use api to fetch thumbnail, http://vimeo.com/api/v2/video/103939722.json
		@_elements = []

		iframes = @$el.find("iframe")
		iframes.each (index, el) =>
			@_elements.push new CaseGalleryIFrame
				el: el

		images = @$el.find("img")
		images.each (index, el) =>
			Type = if relax.browser.isMobile() then CaseMobileGalleryImage else CaseGalleryImage
			img = new Type()
			@_elements.push img

			img.load $(el).attr("src"), relax.dom.bind(@, @onImageLoaded), true
			$(el).attr("src", "")

			$(img.data).attr("width", $(el).attr("width"))
			$(img.data).attr("height", $(el).attr("height"))

			$(el).replaceWith(img.data)

		@resize()


	onImageLoaded: ->
		@_loaded = true
		for el in @_elements
			@_loaded = el.loaded

		@resize()

	resize: ->
		y = 0
		w = 0
		h = 0

		galleryWidth = @$el.width()
		galleryHeight = galleryWidth * (9/16)
		maxElHeight = window.application.stageModel.h * 0.6
		maxY = 0

		galleryHeight = Math.min(800, Math.max(350, maxElHeight))
		_galleryHeight = 0

		imageSizeRatio = if @_elements.length is 1 then 1 else 0.5

		#resize nodes
		_maxNodeHeight = if imageSizeRatio is 1 then maxElHeight else maxElHeight * 0.6

		# TODO: hvis højformat billed, så 0.8 - 0.9!
		_nodeWidth = if @_elements.length is 1 then galleryWidth else galleryWidth * 0.5

		for node in @_elements

			element = if node.data then node.data else node.el
			_elWidth = parseInt($(element).attr("width"))
			_elHeight = parseInt($(element).attr("height"))
			aspectRatio = _elHeight / _elWidth

			w = _nodeWidth
			h = _nodeWidth * aspectRatio

			if h > _maxNodeHeight
				aspectRatio = _elWidth / _elHeight
				h = _maxNodeHeight
				w = h * aspectRatio
			
			index = @_elements.indexOf(node)

			isLandscape = h < w

			y = 0
			x = 0

			hh = galleryHeight * imageSizeRatio
			ww = galleryWidth * imageSizeRatio

			if imageSizeRatio is 1
				x = ww * 0.5 - w * 0.5
				y = hh * 0.5 - h * 0.5
			else

				if isLandscape
					# landscape
					if index is 0 or index is 2
						y = hh - h * 0.5
					else if index is 3
						y = galleryHeight - h

					if index is 1 or index is 3
						x = ww - w * 0.5
					else if index is 2
						x = ww
					else if index is 0
						x = ww - w
				else
					# portait
					if index is 0 or index is 2
						y = hh - h * 0.5
					else if index is 3
						y = galleryHeight - h

					if index is 0
						x = galleryWidth * 0.475 - w
					else if index is 1 or index is 3
						x = galleryWidth * 0.5 - w * 0.5
						x += if index is 1 then (galleryWidth * -0.05) else (galleryWidth * 0.05)
					else if index is 2
						x = galleryWidth * 0.525

				# distortion
				if index is 0
					y += galleryHeight * 0.05
				else if index is 1 or index is 3
					y += if index is 1 then 15 else -15
				else if index is 2
					y -= galleryHeight * 0.05

			if y + h > _galleryHeight
				_galleryHeight = y + h

			node.position x, y, w, h

		@$el.css
			"height": _galleryHeight


	dealoc: ->
		for el in @_elements
			el.dealoc?()

		@unbind()
		@remove()










class MobileCaseGallery extends CaseGallery

	@TICK_TIMER: 3000

	#TODO: Fjern timer ved side fjernelse!

	_ticker: null
	_tickIndex: 0

	events:
		"click": "onClick"

	onImageLoaded: ->
		super()

		if @_loaded
			@startTick()

	onClick: (event) ->
		event.preventDefault()
		@startTick()

	setEnabled: (value) ->
		clearTimeout @_ticker if @_ticker

		@_enabled = value
		if @_enabled
			@startTick 0

	startTick: (forceIndex = -1) ->
		@makeATick forceIndex
			
	makeATick: (forceIndex = -1) ->
		clearTimeout @_ticker if @_ticker

		@_ticker = setTimeout =>
			@makeATick forceIndex
		, MobileCaseGallery.TICK_TIMER

		@switchImage forceIndex

	switchImage: (forceIndex = -1) ->
		if forceIndex is -1
			@_tickIndex++
			if @_tickIndex > @_elements.length - 1
				@_tickIndex = 0
		else
			@_tickIndex = forceIndex

		for el in @_elements
			index = @_elements.indexOf el

			_el = if el.data then el.data else el.el

			$(_el).css
				"z-index": @_elements.length - index

			$(_el).removeClass "mobile-prep-animate-in, mobile-animate-in"

		_el = $(if @_elements[@_tickIndex].data then @_elements[@_tickIndex].data else @_elements[@_tickIndex].el )
		_el.css
			"z-index": index + 1

		_el.addClass "mobile-prep-animate-in"
		setTimeout =>
			_el.addClass "mobile-animate-in"
		, 0
		
	resize: (model) ->

		elWidth = @$el.width()
		h = 0

		for el in @_elements
			_el = if el.data then el.data else el.el
			aspectRatio = parseInt($(_el).attr("height")) / parseInt($(_el).attr("width"))
			h = aspectRatio * elWidth

			el.position(0, 0, elWidth, h)

		@$el.css
			"height": h


class CaseGalleryIFrame extends Backbone.View
	position: (x, y, w, h) ->
		@$el.css
			"left": x
			"top": y
			"width": w
			"height": h
	dealoc: ->
		@unbind()
		@remove()
