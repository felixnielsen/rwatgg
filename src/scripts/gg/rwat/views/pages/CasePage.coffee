class CasePage extends DefaultPage

	_y: 0
	_yTarget: 0
	_initResizeTimeout: null

	_sections: null

	render: ->
		super()

		@$el.find("span").attr "style", ""

		if Modernizr.touch
			@_swiper = new relax.tools.DragAndSwipe @$el
			@_swiper.addDrag relax.dom.bind(@, @onDrag)
		else
			window.application.stageModel.bind StageModel.SCROLL, @onScroll, @

		@_sections = []
		@$el.find("section.content").each (index, el) =>
			@_sections.push new CaseSection
				el: el

		@resize window.application.stageModel

		@$el.find(".gallery > p").remove() #remove wierd vimeo shiiat

		# call the render method to set initial visible on sections
		setTimeout =>
			@renderQueCall()
		, 0

	resize: (model) ->
		sectionYPosition = parseInt(@$el.find(".content").css("margin-top"), 10)
		sectionYPosition = 0 if isNaN(sectionYPosition)
		
		for section in @_sections
			section.resize()
			section.setTop(sectionYPosition)

			mb = parseInt(section.$el.css("margin-bottom"), 10)
			mb = 0 if isNaN(mb)

			sectionYPosition += mb
			sectionYPosition += section.$el.height()

		setTimeout =>
			@_height = @$el.height()

			@_swiper?.setBoundries
				minSwipeY: (@_height - model.halfh) * -1
				maxSwipeY: 1
		
			window.application.scrollHandler.setHeight @_height
		, 0

	onDrag: (obj) -> @setYTarget obj.y

	onScroll: (obj) ->
		if @_height isnt @$el.height()
			# just a safety check
			@resize window.application.stageModel

		obj.event?.preventDefault()
		percent = window.application.scrollHandler.getPercent()
		@setYTarget percent * (window.application.stageModel.h - @_height)

	setYTarget: (value) ->
		@_yTarget = value

		if !relax.tools.renderQue.has @
			relax.tools.renderQue.add @

	dealoc: ->
		clearTimeout @_initResizeTimeout if @_initResizeTimeout
		for section in @_sections
			section.dealoc()
		super()

	animateOut: (newModel) ->

		window.application.stageModel.unbindAll @

		@_swiper?.dealoc()

		for section in @_sections
			section.remove()
		
		setTimeout =>
			@dealoc()
		, 1000


	renderQueCall: ->
		t = @_yTarget - @_y

		if Math.abs(Math.round(t)) > 0
			@_y += t * ScrollHandler.TIME_SCROLL_SPEED
		else
			relax.tools.renderQue.remove @

		for section in @_sections
			section.setY @_y


# each content section
class CaseSection extends Backbone.View

	_height: 0
	_gallery: null
	_visible: false
	_removing: false

	initialize: -> @render()
	
	render: ->
		if @$el.find(".gallery").length > 0
			Type = if relax.browser.isMobile() then MobileCaseGallery else CaseGallery
			@_gallery = new Type
				el: @$el.find(".gallery")[0]

		startX = window.application.stageModel.w * -1
		rotate = -90
		@$el.find(".text, h1").css
			"transform": "translateX(" + startX + "px) rotateY(" + rotate + "deg) scale(0.1, 1)"
		
		@$el.find(".gallery img").css
			"transform": "translateX(" + startX + "px) rotateY(" + rotate + "deg) skew(-12deg, 0deg) scale(0.1, 1)"
		@$el.find(".gallery iframe").css
			"transform": "translateX(" + startX + "px) rotateY(" + rotate + "deg) skew(-12deg, 0deg) scale(0.1, 1)"

	setY: (value) ->
		if @_removing then return

		test1 = value + @top + @_height
		test2 = value + @top

		if @_visible
			# if visible then we animate the element, else we leave it be
			@$el.css
				"transform": "translate3d(0px, " + value + "px, 0px)"

		# check so we only move elements that are within the screen.
		@setVisibility test1 > 0 and test2 < window.application.stageModel.h

	setVisibility: (value) ->
		if @_visible isnt value
			@_visible = value
			if @_visible
				# load in the vimeo film, as they are quite heavy to initialize... aternative we have a video file
				@$el.addClass "visible"

				@_gallery?.setEnabled? true
			else
				@$el.removeClass "visible"

				@_gallery?.setEnabled? false

			if !@$el.hasClass("show")
				setTimeout =>
					@$el.addClass("show")
				, 100

	remove: ->
		@_removing = true
		
		if @_visible
			@$el.removeClass "show"
		else
			@$el.removeClass "visible"

	dealoc: ->
		@_gallery?.dealoc()
		@unbind()
		@remove()

	setTop: (value) ->
		@top = value
		@_height = @$el.height()

	resize: -> 
		@_height = @$el.height()
		@_gallery?.resize()















