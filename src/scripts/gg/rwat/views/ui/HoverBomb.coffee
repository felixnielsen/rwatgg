class HoverBomb
	_el: null
	constructor: (el) ->
		@_el = el

		@_el.append('<div class="background"></div><div class="svg"></div>')

		@_bg = @_el.find(".background")[0]
		@_label = @_el.find(".label")[0]
	
	over: ->
		# clear what previously was there.
		@_el.find(".svg").html("")

		w = @_el.width()
		h = @_el.height()

		draw = SVG(@_el.find(".svg")[0]).size(w, h)
		x = 0
		i = 0

		while x < w
			f = (x) =>
				#closure!

				reach = relax.math.lerp(Math.random(), 0, w)

				if x <= 0
					# place on 0 and random y
					p1x = x
					p1y = relax.math.lerp(Math.random(), -10, h + 10)

					p2x = x + reach
					p2y = if Math.random() > 0.5 then Math.random() * -10 else h + (Math.random() * 10)
				else
					p1x = x
					p1y = if Math.random() > 0.5 then Math.random() * -10 else h + (Math.random() * 10)

					p2x = x + reach
					p2y = if p1y >= h then Math.random() * -10 else h + (Math.random() * 10)

				line = draw.line(p1x, p1y, p1x, p1y).stroke({ width: 1, color: window.application.colors.get("secondary") })

				obj =
					p: 0

				p2xtarget = p2x - p1x
				p2ytarget = p2y - p1y

				TweenMax.to obj, 0.2,
					p: 1
					delay: (i++) * 0.06
					ease: "easeInCubic"
					onUpdate: ->
						line.plot(p1x, p1y, p1x + (p2xtarget * obj.p), p1y + (p2ytarget * obj.p))
					onComplete: ->

						obj.p = 0
						ease: "easeOutCubic"
						TweenMax.to obj, 0.2,
							p: 1
							onUpdate: ->
								line.plot(p1x + (p2xtarget * obj.p), p1y + (p2ytarget * obj.p), p2x, p2y)
			f(x) #call closure, so we can wrap the local vars

			x += relax.math.lerp(Math.random(), 0, w * 0.5)

		TweenMax.killTweensOf @_bg
		TweenMax.killTweensOf @_label

		TweenMax.to @_label, 0.4,
			opacity: 0
			ease: "easeOutCubic"

		TweenMax.set @_label,
			delay: 0.2
			"transform": "translateX(-10px)"

		TweenMax.to @_label, 0.2,
			delay: 0.3
			opacity: 1
			color: window.application.colors.get("primary")
			ease: "easeOutExpo"
			"transform": "translateX(0px)"

		TweenMax.to @_bg, 0.2,
			opacity: 1
			background: window.application.colors.get("secondary")
			delay: 0.3
			ease: "easeOutCubic"

	out: ->
		TweenMax.killTweensOf @_bg
		TweenMax.killTweensOf @_label

		TweenMax.to @_bg, 0.2,
			opacity: 0
			ease: "easeOutCubic"

		TweenMax.to @_label, 0.2,
			color: window.application.colors.get("secondary")
			opacity: 1
			"transform": "translateX(0px)"
			ease: "easeOutExpo"
			onComplete: =>
				#remove the color so so the overall color theme can act in
				$(@_label).css "color", ""

