caseSpherePositions = [
		x: 0.140625
		y: 0.74025974
		z: -1000
	,
		x: 0.234375
		y: 0.68359375
		z: -1000
	,
		x: 0.34453125
		y: 0.6953125
		z: -1000
	,
		x: 0.44609375
		y: 0.575520833
		z: -1000
	,
		x: 0.571875
		y: 0.450520833
		z: -1000
	,
		x: 0.60703125
		y: 0.216145833
		z: -1000
	,
		x: 0.75625
		y: 0.227864583
		z: -1000
	,
		x: 0.88125
		y: 0.109375
		z: -1000
]

caseSphereCalculatedVectors = []

for sphereIndex in [0..window.caseSpherePositions.length-2] by 1
	v1 = window.caseSpherePositions[sphereIndex]
	v2 = window.caseSpherePositions[sphereIndex + 1]

	pointsInBetween = 10
	x = v2.x - v1.x
	y = v2.y - v1.y

	for i in [0..pointsInBetween] by 1
		percent = i / pointsInBetween

		xvector = v1.x + (x * percent)
		yvector = v1.y + (y * percent)

		if !isNaN(xvector) and !isNaN(yvector)
			window.caseSphereCalculatedVectors.push
				x: xvector
				y: yvector


class CaseSphere extends THREE.Mesh

	_index: null
	_scene: null
	_camera: null
	_projector: null

	# vectors
	_rotationSpeedVector: null
	_targetVector: null
	_animatingVector: null

	# texture
	material: null
	_color: "#ffffff"

	# name says it all
	particles: null
	_lines: null
	_nextMesh: null
	_line: null
	_lines: null

	# data
	_pageURL: ""
	_active: false
	_hasAnimatedIn: false
	_allowScrollManipulation: true

	_hoverTimer: 0
	_numCases: null

	# speed
	@SPEED_QUICK: 0.2
	@SPEED_NORMAL: 0.09
	@SPEED_SLOW: 0.02
	@SPEED_ULTRA_SLOW: 0.005
	_speed: 0.09 # @SPEED_NORMAL

	constructor: (options) ->
		@_index = options.index
		@_scene = options.scene
		@_camera = options.camera
		@_projector = options.projector
		@_color = options.color

		@_pageURL = options.url

		_.extend @, Backbone.Events

		@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01))

		@material = new THREE.MeshBasicMaterial
			wireframe: true
			transparent: true

		@geometry = new THREE.SphereGeometry 80, 5, 5

		# -> after creation
		super @geometry, @material

		@_targetVector = new THREE.Vector3 0, 0, -10000
		@_animatingVector = @_targetVector.clone()

		numParticles = Math.ceil(relax.math.lerp(Math.random(), 5, 15))
		@particles = new DotParticles @, numParticles

		@buildLines()

		@_numCases = options.numCases
		@scale.set(0.001, 0.001, 0.001)

		# @_faceBlast = new ImageBlast
		# 	geometry: @geometry.clone()
		# 	scene: @_scene
		# 	color: "#ff0000"

		window.application.pages.on "updated", @onPageChange, @

	updateVisible: (percent) ->
		if @_allowScrollManipulation and @_numCases > 0
			range = Math.max(0.001, Math.min(percent / (@_index / @_numCases), 1))
		else
			range = 1

		if @scale.x isnt range
			@scale.x = range
			@scale.y = range
			@scale.z = range

	buildLines: ->
		@_lines = []
		movetonextparticle = true
		l = @particles._system.geometry.vertices.length - 1
		for i in [0...l] by 1
			# v1 = @geometry.vertices[i] # verticies on sphere

			v1 = if i + 1 > @particles._system.geometry.length then 0 else @particles._system.geometry.vertices[i+1] # next particle!
			v2 = @particles._system.geometry.vertices[i] # particle

			@_lines.push new SphereLine v1, v2,
				index: i
				numLines: l
				lineAlpha: 0.6
				color: @_color
				blinkInterval: 50
			@add @_lines[@_lines.length-1]

	onPageChange: (model, dto) ->
		@_allowScrollManipulation = dto.type is "home"
		type = dto.type
		url = dto.url

		# @_faceBlast.stopLoop()

		activeValue = @_pageURL.indexOf(url) isnt -1
		activeValue = url isnt "" if activeValue
		activeValue = url.length > 2 if activeValue

		@setActive activeValue, type is "case"

	getColor: -> @material.color

	electricute: ->
		if @_active then return

		@_speed = CaseSphere.SPEED_NORMAL
		@particles._system.visible = true

		clearTimeout = @_hoverTimer
		@_hoverTimer = setTimeout =>
			@startBlink(true)
			# @_faceBlast.react(false, true)
		, 500

		@_animatingVector.x = 0
		@_animatingVector.y = 0
		@_animatingVector.z = -1000

	unelectricute: ->
		if @_active then return

		@_speed = CaseSphere.SPEED_SLOW
		@particles._system.visible = false

		clearTimeout @_hoverTimer
		# @_faceBlast.stopLoop()
		@stopBlink()

		@_animatingVector.x = @_targetVector.x
		@_animatingVector.y = @_targetVector.y
		@_animatingVector.z = @_targetVector.z

	startBlink: (runOnce)->
		runOnce = true
		# show image in verticies
		func = =>
			clearTimeout @_hoverTimer
			t = if runOnce then 0 else relax.math.lerp(Math.random(), 1000, 5000)
			@_hoverTimer = setTimeout =>
				# fire in the hole
				line.unblink() for line in @_lines if @_lines
				line.blink() for line in @_lines if @_lines
				if !runOnce
					func(runOnce)
			, t

		func()

	stopBlink: ->
		clearTimeout @_hoverTimer
		line.unblink() for line in @_lines if @_lines

	setActive: (value, isCasePage = false) ->
		@_active = value

		if @_active
			@_animatingVector.x = 0
			@_animatingVector.y = 0

			# @_faceBlast.react(false, false)

			@_animatingVector.z = -25
			@startBlink false
			@_speed = CaseSphere.SPEED_NORMAL
			@particles._system.visible = true

			@visible = true
			@_line?.visible = true

			TweenLite.to @material, 1,
				opacity: 0.35
			if @_line
				TweenLite.to @_line.material, 1,
					opacity: 0.35
		else
			@_animatingVector.x = @_targetVector.x
			@_animatingVector.y = @_targetVector.y
			@_animatingVector.z = @_targetVector.z

			TweenLite.to @material, 1,
				opacity: if isCasePage then 0.8 else 1
			if @_line
				TweenLite.to @_line.material, 1,
					opacity: if isCasePage then 0.5 else 1
			# @material.opacity = 
			# @_line?.material.opacity = if isCasePage then 0.2 else 1

			# @visible = !isCasePage
			# @_line?.visible = !isCasePage

			@stopBlink()
			@particles._system.visible = false

		range = if !@_active then 0.01 else 0.0003
		@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), range * -1, range), relax.math.lerp(Math.random(), range * -1, range), relax.math.lerp(Math.random(), range * -1, range))

	connectMesh: (nextMesh) ->
		@_nextMesh = nextMesh
		@setupLine()

	setupLine: () ->
		pos = @_nextMesh.position

		@lineGeometry = new THREE.Geometry()

		# get random verticie
		@_bla = Math.floor(Math.random() * @geometry.vertices.length)
		@_blabla = Math.floor(Math.random() * @_nextMesh.geometry.vertices.length)

		@lineGeometry.vertices.push @position.clone()
		@lineGeometry.vertices.push new THREE.Vector3(pos.x, pos.y, pos.z)

		@lineMaterial = new THREE.LineBasicMaterial
			color: "#ffffff" #WorldDot.INITIAL_SCREEN_POS[@_index % WorldDot.INITIAL_SCREEN_POS.length].color
			transparent: true
			linewidth: 1

		@_line = new THREE.Line @lineGeometry, @lineMaterial
		@_scene.add @_line

	resize: (w, h) ->
		hw = w * 0.5
		hh = h * 0.5

		percent = @_index / @_numCases
		vectorIndex = Math.floor(percent * (window.caseSphereCalculatedVectors.length - 1))
		posVector = window.caseSphereCalculatedVectors[vectorIndex]

		if posVector
			@_targetVector.x = (hw * -1) + (posVector.x * w)
			@_targetVector.y = hh + (posVector.y * h * -1)

			if !@_active or (!@_hasAnimatedIn and !@_active)
				@_animatingVector.x = @_targetVector.x
				@_animatingVector.y = @_targetVector.y
				@_animatingVector.z = @_targetVector.z

				if !@_hasAnimatedIn
					@position.x = @_animatingVector.x
					@position.y = @_animatingVector.y
					@position.z = @_animatingVector.z


		lineIndex = window.caseSpherePositions.length * (@_index / @_numCases)

		@setupLine() if @_nextMesh and !@_line

		@render()

	render: ->
		x = @position.x + (@_animatingVector.x - @position.x) * @_speed
		y = @position.y + (@_animatingVector.y - @position.y) * @_speed
		z = @position.z + (@_animatingVector.z - @position.z) * @_speed

		#do some in between calculations here, like the zoom value etc.
		@position.x = x
		@position.y = y
		@position.z = z

		@rotation.x += @_rotationSpeedVector.x
		@rotation.y += @_rotationSpeedVector.y
		@rotation.z += @_rotationSpeedVector.z

		@renderLine() if @lineGeometry

		# @_faceBlast.render(@position, @rotation)

	renderLine: ->
		# TODO: gør så linierne er i midten ved ikke active state
		# vert1 = @position
		# vert2 = @_nextMesh.position
		# @updateMatrixWorld() # no need to?
		vert1 = @localToWorld(@geometry.vertices[@_bla].clone())
		vert2 = @_nextMesh.localToWorld(@_nextMesh.geometry.vertices[@_blabla].clone())

		@lineGeometry.vertices[0].x = vert1.x
		@lineGeometry.vertices[0].y = vert1.y
		@lineGeometry.vertices[0].z = vert1.z

		@lineGeometry.vertices[1].x = relax.math.lerp(@scale.x, @position.x, vert2.x)
		@lineGeometry.vertices[1].y = relax.math.lerp(@scale.y, @position.y, vert2.y)
		@lineGeometry.vertices[1].z = relax.math.lerp(@scale.z, @position.z, vert2.z)

		@_line.geometry.verticesNeedUpdate = true


