class SphereLine extends THREE.Line

	_timer: null
	_index: null
	_numLines: -1
	_timer: 0
	_alpha: 0
	_blinkInterval: 0

	constructor: (v1, v2, options) ->
		@_index = options.index
		@_numLines = options.numLines
		@_alpha = if options.lineAlpha then options.lineAlpha else 0.4
		@_blinkInterval = if options.blinkInterval then options.blinkInterval else 20

		@geometry = new THREE.Geometry()
		@geometry.vertices.push v1
		@geometry.vertices.push v2

		@material = new THREE.LineBasicMaterial
			color: if options.color then options.color else "#ffffff"
			linewidth: 1
			transparent: true

		super @geometry, @material

		@visible = false

	blink: ->
		clearTimeout @_timer
		@_timer = setTimeout =>
			@visible = true
			@_timer = setTimeout =>
				@material.opacity = @_alpha
				@material.needsUpdate = true
			, 100
		, @_index * @_blinkInterval

	unblink: ->
		clearTimeout @_timer
		@material.opacity = 0
		@visible = false
