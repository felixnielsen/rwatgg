class ThreeWorld

	@_collection: null

	rwatggSphere: null
	_rotateXTarget: 0
	_rotateX: 0
	_xTarget: 0
	_x: 0
	_caseListAnimationProgress: 0


	constructor: (options) ->
		@camera = null
		@_renderer = null
		@meshes = []

		@_projector = new THREE.Projector()

		window.application.pages.on "updated", @onPageChange, @

		@init(options)

	onPageChange: (model, dto) ->
		type = dto.type
		url = dto.url

		if type is "filter"
			@_xTarget = 50
		else 
			@_xTarget = 0

	init: (options) ->

		@scene = new THREE.Scene()

		cameraViewNear = 0.01
		cameraViewFar = 12000
		cameraFOV = 50

		@camera = new THREE.PerspectiveCamera cameraFOV, window.innerWidth / window.innerHeight, cameraViewNear, cameraViewFar

		# setup rendere
		if Detector.webgl
			@_renderer = new THREE.WebGLRenderer
				antialias: true
				alpha: true
		else
			@_renderer = new THREE.CanvasRenderer
				antialias: true
				alpha: true

		$(@_renderer.domElement).attr "id", "wires"
		@_renderer.setSize window.innerWidth, window.innerHeight
		# @_renderer.setClearColor 0xffffff, 0

		document.body.appendChild @_renderer.domElement

		if !relax.browser.isMobile()# and !relax.browser.isTablet()
			for i in [0..options.cases.length] by 1
				if $(options.cases[i]).attr("href")
					@buildMesh i,
						url: $(options.cases[i]).attr("href")
						numCases: options.cases.length - 1
						color: $(options.cases[i]).data("color")

		@rwatggSphere = new RwatggSphere
			scene: @scene
			camera: @camera
			projector: @_projector

		@meshes.push @rwatggSphere
		@scene.add @rwatggSphere


		#add a light for our textures, no need for shadows!
		@scene.add new THREE.AmbientLight(0xffffff)

		# dirLight = new THREE.DirectionalLight( 0xffffff, 1 )
		# dirLight.position.set( -1, 1.75, 1 )
		# dirLight.position.multiplyScalar( 50 )
		# @scene.add( dirLight )

		relax.tools.renderQue.add @

		window.application.stageModel.bind StageModel.RESIZE, @resize, @


	buildMesh: (i, options) ->
		dot = new CaseSphere
			index: i
			color: options.color
			numCases: options.numCases
			url: options.url
			parentMesh: if @meshes.length > 0 then @meshes[i-1] else null
			scene: @scene
			camera: @camera
			projector: @_projector

		# dot.on "animated-in", @onDotAnimatedIn, @

		@meshes.push dot
		@scene.add dot

		if @meshes[i-1]
			@meshes[i-1].connectMesh @meshes[i]

		delayIndex = if i > @_numDots then ((i - @_numDots) + 5) else i
		# @meshes[i].show delayIndex

	getMesh: (index) -> @meshes[index]

	peakABoo: ->
		@rwatggSphere.updateVisible true

		updateMeshes = =>
			for mesh in @meshes
				mesh.updateVisible?(@_caseListAnimationProgress)

		TweenLite.to @, 1,
			easing: "easeOutCubic"
			_caseListAnimationProgress: 1
			delay: 1
			onUpdate: => updateMeshes()
			onComplete: => updateMeshes()

		window.application.stageModel.bind StageModel.SCROLL, @scroll, @, true

	resize: (model) ->

		@_renderer.setSize model.w, model.h

		@camera.aspect = model.w / model.h
		@camera.updateProjectionMatrix()

		# get the width and height of the cameras fov, useful to place elements evengly around the visible area (eye)
		cameraStaticZ = 14000 # should be the same Z as the elements that are being placed
		vFOV = @camera.fov * Math.PI / 180 # convert vertical fov to radians
		height = 2 * Math.tan( vFOV / 2 ) * cameraStaticZ # visible height
		width = height * @camera.aspect # visible width

		for dot in @meshes
			dot.resize(width, height)

		@renderQueCall()

	scroll: ->
		p = window.application.scrollHandler.getPercent()
		@_rotateXTarget = -0.025 + (p * 0.1)

	renderQueCall: ->
		for mesh in @meshes
			mesh.render()

		@_renderer.render @scene, @camera

		@_rotateX += (@_rotateXTarget - @_rotateX) * 0.02
		@camera.rotation.x = @_rotateX

		@_x += (@_xTarget - @_x) * 0.01
		@camera.position.x = @_x






