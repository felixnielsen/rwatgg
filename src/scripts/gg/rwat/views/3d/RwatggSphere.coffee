class RwatggSphere extends THREE.Mesh

	_scene: null
	_camera: null
	_projector: null

	# vectors
	_rotationSpeedVector: null
	_targetVector: null
	_currentZ: 100

	# texture
	material: null
	_color: "#ffffff"

	# name says it all
	particles: null
	_eletricuteTimer: 0

	_lines: null

	_visible: false

	constructor: (options) ->
		@_scene = options.scene
		@_camera = options.camera
		@_projector = options.projector

		_.extend @, Backbone.Events

		@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01))

		@material = new THREE.MeshBasicMaterial
			wireframe: true

		# value = Math.ceil(relax.math.lerp(Math.random(), 4, 8))
		@geometry = new THREE.SphereGeometry 80, 5, 5

		super @geometry, @material

		@_targetVector = new THREE.Vector3 0, 0, @_currentZ
		if relax.browser.isMobile() or relax.browser.isTablet()
			@position.z = @_targetVector.z = @_currentZ = -100
		else
			@position.z = @_currentZ


		window.application.pages.on "updated", @onPageChange, @

		@particles = new DotParticles @, @geometry.vertices.length * 3
		@buildLines()

		if !Modernizr.touch
			@_faceBlast = new ImageBlast
				geometry: @geometry.clone()
				scene: @_scene
				color: "#ffffff"

	updateVisible: (show) ->	
		if show and !@_visible
			@_visible = true
			@_targetVector.z = @_currentZ
			@hiFromMenuItem(0xffffff, true)
		# else if percent < 0.01 and @_visible
		# 	@_visible = false
		# 	@_targetVector.z = 100

	buildLines: ->
		#TODO: do have lines or not to have lines, that is the question..!
		@_lines = []
		movetonextparticle = true
		l = @geometry.vertices.length
		for i in [0...l] by 1
			# v1 = @geometry.vertices[i] # verticies on sphere

			v1 = if i + 1 > @particles._system.geometry.length then 0 else @particles._system.geometry.vertices[i+1] # next particle!
			v2 = @particles._system.geometry.vertices[i] # particle

			@_lines.push new SphereLine v1, v2,
				index: i
				numLines: l
			@add @_lines[@_lines.length-1]

	onPageChange: (model, dto) ->
		type = dto.type
		url = dto.url

		# set z and rotation speed
		if type is "home" or type is "filter"
			newZ = if relax.browser.isMobile() then -400 else -300
			isZDifferent = @_currentZ isnt newZ
			@_currentZ = newZ
			if isZDifferent
				@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01))
		else if type is "text"
			@_currentZ = if dto.url is "contact" then -350 else -150
			@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), -0.005, 0.005), relax.math.lerp(Math.random(), -0.005, 0.005), relax.math.lerp(Math.random(), -0.005, 0.005))
		else
			@_currentZ = 500
			@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), -0.001, 0.001), relax.math.lerp(Math.random(), -0.001, 0.001), relax.math.lerp(Math.random(), -0.001, 0.001))

		@_color = new THREE.Color window.application.colors.get("secondary")

		@_targetVector.z = @_currentZ if @_visible

		# if url is "contact"
		@material.color = @_color
		@particles.updateColorTo @_color

	getColor: -> @material.color

	hiFromMenuItem: (color, quick = false) ->
		if @_faceBlast
			clearTimeout @_eletricuteTimer if @_eletricuteTimer
			@_eletricuteTimer = setTimeout =>
				@_faceBlast.react(true, false, color)
				window.application.sounds.play("hover")
			, if quick then 0 else 150

	unhiFromMenuItem: -> clearTimeout @_eletricuteTimer if @_eletricuteTimer

	electricute: -> line.blink() for line in @_lines
	unelectricute: -> line.unblink() for line in @_lines

	resize: (w, h) ->
		hw = w * -0.5
		hh = h * 0.5

		@_targetVector.x = hw + (w * 0.5)
		@_targetVector.y = hh + ((h * -1) * 0.5)

		@render()

	render: ->
		x = @position.x + (@_targetVector.x - @position.x) * 0.06
		y = @position.y + (@_targetVector.y - @position.y) * 0.06
		z = @position.z + (@_targetVector.z - @position.z) * 0.06

		#do some in between calculations here, like the zoom value etc.
		@position.x = x
		@position.y = y
		@position.z = z

		@rotation.x += @_rotationSpeedVector.x
		@rotation.y += @_rotationSpeedVector.y
		@rotation.z += @_rotationSpeedVector.z

		@_faceBlast?.render(@position, @rotation)






class ImageBlast 
	_materialIndexs: null
	_randomIndexs: null
	_runningIndex: -1
	_faceMaterial: null
	_materials: null
	_numFaces: -1
	_reacting: false
	_loaded: false
	_scene: null
	_geom: null
	mesh: null
	constructor: (options) ->

		@mesh = new THREE.Mesh()

		if !options.color
			options.color = "#ffffff"

		@_geom = options.geometry
		@_scene = options.scene

		@_numFaces = options.geometry.faces.length
		@_materials = [
			new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, ambient: new THREE.Color( options.color ), transparent: true, opacity: 0 } ),
			new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, ambient: new THREE.Color( options.color ), transparent: true, opacity: 0.85 } ),
			new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, ambient: new THREE.Color( options.color ), transparent: true, opacity: 0.9 } ),
			new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, ambient: new THREE.Color( options.color ), transparent: true, opacity: 0.95 } ),
			new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, ambient: new THREE.Color( options.color ), transparent: true, opacity: 0.75 } ),
			new THREE.MeshLambertMaterial( { side: THREE.DoubleSide, ambient: new THREE.Color( options.color ), transparent: true, opacity: 0.80 } )
		]

		@_faceMaterial = new THREE.MeshFaceMaterial(@_materials)
		
		@_loaded = true

		@_materialIndexs = []
		for i in [0...@_numFaces] by 1
			num = 1 + (i % (@_materials.length - 2)) #Math.ceil((Math.random() * (@_materials.length - 2)))
			@_materialIndexs.push(num)

	stopLoop: ->
		TweenLite.killTweensOf @
		clearTimeout @_reactTimer if @_reactTimer
		@_reactTimer = null
		@mesh?.visible = false
		@_reacting = false

	react: (makeRandom = false, doLoop = false, color = null) ->
		@stopLoop()
		@mesh?.visible = true

		if color
			for mat in @_materials
				mat.ambient.setHex color
			
		@_reacting = true

		@percent = 0
		@_runningIndex = -1

		if makeRandom
			@_randomIndexs = []
			for i in [0...@_numFaces] by 1
				@_randomIndexs.push(i)

			@_randomIndexs.sort -> return .5 - Math.random()

		TweenLite.to @, 0.6,
			percent: 2
			ease: "linear"
			onUpdate: => @update()
			onComplete: =>
				if doLoop
					clearTimeout @_reactTimer if @_reactTimer
					if @_reacting
						@_reactTimer = setTimeout =>
							@react(makeRandom, doLoop)
						, relax.math.lerp(Math.random(), 4000, 8000)
				else
					@mesh?.visible = false

	update: ->
		p = @percent
		if p > 1
			p = 2-p
		else
			p = p

		nextIndex = Math.round(@_numFaces * p)

		if nextIndex isnt @_runningIndex
			@_runningIndex = nextIndex

			@_scene.remove @mesh if @mesh

			@mesh = new THREE.Mesh(@_geom.clone(), @_faceMaterial)
			@_scene.add @mesh

			for i in [0...@_numFaces] by 1
				if @_randomIndexs
					face = @mesh.geometry.faces[@_randomIndexs[i]]
				else
					face = @mesh.geometry.faces[i]

			
				if i < @_runningIndex and @percent <= 1 or i > @_numFaces - @_runningIndex and @percent > 1
					face.materialIndex = @_materialIndexs[i]
				else
					face.materialIndex = 0
			
			@mesh.geometry.colorsNeedUpdate = true


	render: (position, rotation) ->
		if @_loaded
			@mesh.position.x = position.x
			@mesh.position.y = position.y
			@mesh.position.z = position.z

			@mesh.rotation.x = rotation.x
			@mesh.rotation.y = rotation.y
			@mesh.rotation.z = rotation.z

