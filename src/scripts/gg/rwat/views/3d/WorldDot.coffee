class WorldDot extends THREE.Mesh

	geometry: null
	material: null
	lineMaterial: null

	line: null

	lineGeometry: null
	nextMesh: null

	color: null

	_active: false

	_scene: null
	_camera: null
	_projector: null
	_index: -1

	_targetVector: null

	particles: null
	_rotationSpeedVector: null

	_step: 0

	@INITIAL_SCREEN_POS:[
			x: 0.140625
			y: 0.74025974
			z: -1000
			color: "#00ff00"
		,
			x: 0.234375
			y: 0.68359375
			z: -1000
			color: "#ff0000"
		,
			x: 0.34453125
			y: 0.6953125
			z: -1000
			color: "#ff00ff"
		,
			x: 0.44609375
			y: 0.575520833
			z: -1000
			color: "#00ffff"
		,
			x: 0.571875
			y: 0.450520833
			z: -1000
			color: "#0000ff"
		,
			x: 0.60703125
			y: 0.216145833
			z: -1000
			color: "#00ff00"
		,
			x: 0.75625
			y: 0.227864583
			z: -1000
			color: "#ff0000"
		,
			x: 0.88125
			y: 0.109375
			z: -1000
			color: "#ff00ff"
	]

	constructor: (options) ->
		super( @geometry, @material )

		@_index = options.index
		@_scene = options.scene
		@_camera = options.camera
		@_projector = options.projector

		@color = options.color

		@_rotationSpeedVector = new THREE.Vector3(relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01), relax.math.lerp(Math.random(), -0.01, 0.01))

		_.extend @, Backbone.Events

		@setGeometry()

		@material = new THREE.MeshBasicMaterial
			color: "#ffffff" #WorldDot.INITIAL_SCREEN_POS[@_index % WorldDot.INITIAL_SCREEN_POS.length].color
			wireframe: true

		@lineMaterial = new THREE.LineBasicMaterial
			color: "#ffffff" #WorldDot.INITIAL_SCREEN_POS[@_index % WorldDot.INITIAL_SCREEN_POS.length].color
			linewidth: 1

		@_targetVector = new THREE.Vector3( 0, 0, 1000 )

		@scale.set(0.001,0.001,0.001) #three js does not like 0.

		@particles = new DotParticles @

		#@_text = $("body").find("#tester"+@_index).eq(0)

	setGeometry: (value = null) ->
		if !value
			value = Math.ceil(relax.math.lerp(Math.random(), 4, 8))

		@geometry = new THREE.SphereGeometry(value, 5, 5)

	connectMesh: (nextMesh) ->
		@nextMesh = nextMesh

	toXYCoords: (pos) ->
		#@_camera.updateMatrixWorld()
		vector = @_projector.projectVector(pos.clone(), @_camera)
		vector.x = (vector.x + 1)/2 * window.innerWidth
		vector.y = -(vector.y - 1)/2 * window.innerHeight
		return vector

	updateColor: (value) ->
		@color = value

		@updateMaterial()

	show: (delayIndex = 0) ->
		TweenLite.to @scale, 0.2,
			x: 1
			y: 1
			z: 1

			delay: delayIndex * 0.1
			ease: "easeInOutCubic"

			onComplete: =>
				@trigger "animated-in", @

	resize: (w, h) ->
		@updateTarget @_step, 
			width: w
			height: h

		if !@line
			@position.x = @_targetVector.x
			@position.y = @_targetVector.y
			@position.z = @_targetVector.z

			@setupLine()

		@render()

	active: (state) ->
		@_active = state

		if @_active
			@_targetVector.x = 0
			@_targetVector.y = @_camera.position.y
			@_targetVector.z = -20#relax.math.lerp(Math.random(), 0, -100)
		else
			@updateTarget(0)

		@particles.createSystem()

		@updateMaterial()

	getColor: -> if @_active then @color else 0xffffff

	updateMaterial: ->

		@material = new THREE.MeshBasicMaterial
			color: @getColor()
			wireframe: true

	updateTarget: (step, options) ->
		if options
			@ww = options.width
			@hh = options.height

		hw = @ww * -0.5
		hh = @hh * 0.5

		@_step = step

		if step is 0
			if @_index < WorldDot.INITIAL_SCREEN_POS.length
				staticx = WorldDot.INITIAL_SCREEN_POS[@_index].x
				staticy = WorldDot.INITIAL_SCREEN_POS[@_index].y
				staticz = WorldDot.INITIAL_SCREEN_POS[@_index].z
			else
				staticx = WorldDot.INITIAL_SCREEN_POS[WorldDot.INITIAL_SCREEN_POS.length - 1].x
				staticy = WorldDot.INITIAL_SCREEN_POS[WorldDot.INITIAL_SCREEN_POS.length - 1].y
				staticz = WorldDot.INITIAL_SCREEN_POS[WorldDot.INITIAL_SCREEN_POS.length - 1].z


			@_targetVector.x = hw + (staticx * @ww)
			@_targetVector.y = hh + (staticy * (@hh * -1))
			@_targetVector.z = staticz

			if @_index >= WorldDot.INITIAL_SCREEN_POS.length
				# rest of the cases
				ni = @_index - 7

				@_targetVector.x += ni * 150
				@_targetVector.y += relax.math.lerp(Math.random(), -20, 20)
				@_targetVector.z += ni * 50


		# else if step is 1
		# 	@_targetVector.x = hw * -0.1 + ((@_index * 200) + relax.math.lerp(Math.random(), -50, 50))
		# 	@_targetVector.y = (@_index * 100) + relax.math.lerp(Math.random(), -20, 20)
		# 	@_targetVector.z = (@_index * -300)

	updateToIndex: (index) ->
		log "align to right side of the browser"

		if @ww and @hh
			hw = @ww * -0.5
			hh = @hh * 0.5

			@_targetVector.x = hw + (0.9 * @ww)
			@_targetVector.y = hh + (0.5 * (@hh * -1))
			@_targetVector.z = @_targetVector.z


	setupLine: ->
		if @nextMesh
			pos = @nextMesh.position

			@lineGeometry = new THREE.Geometry()

			@_bla = Math.floor(Math.random() * @geometry.vertices.length)
			@_blabla = Math.floor(Math.random() * @nextMesh.geometry.vertices.length)

			# @lineGeometry.vertices.push @position.clone()
			# @lineGeometry.vertices.push pos.clone()

			@lineGeometry.vertices.push @position.clone()
			@lineGeometry.vertices.push new THREE.Vector3(pos.x, pos.y, pos.z)

			@line = new THREE.Line @lineGeometry, @lineMaterial
			@_scene.add @line

	_bla: null
	_blabla: null

	render: ->
		x = @position.x + (@_targetVector.x - @position.x) * 0.06
		y = @position.y + (@_targetVector.y - @position.y) * 0.06
		z = @position.z + (@_targetVector.z - @position.z) * 0.06

		#do some in between calculations here, like the zoom value etc.
		@position.x = x
		@position.y = y
		@position.z = z

		@rotation.x += @_rotationSpeedVector.x
		@rotation.y += @_rotationSpeedVector.y
		@rotation.z += @_rotationSpeedVector.z

		if @lineGeometry
			bla = @geometry.vertices[@_bla].clone()
			blabla = @nextMesh.geometry.vertices[@_blabla].clone()

			#update our matrix, and get local to world cords
			@updateMatrixWorld()
			bla = @localToWorld(bla)
			blabla = @nextMesh.localToWorld(blabla)

			@lineGeometry.vertices[0].x = bla.x
			@lineGeometry.vertices[0].y = bla.y
			@lineGeometry.vertices[0].z = bla.z

			@lineGeometry.vertices[1].x = relax.math.lerp(@scale.x, @position.x, blabla.x)
			@lineGeometry.vertices[1].y = relax.math.lerp(@scale.y, @position.y, blabla.y)
			@lineGeometry.vertices[1].z = relax.math.lerp(@scale.z, @position.z, blabla.z)

			@line.geometry.verticesNeedUpdate = true

		if @_text
			_2dVector = @toXYCoords @position
			@_text.css
				"left": _2dVector.x
				"top": _2dVector.y
				"opacity": 1 - (@_index / 14)



# getter setters, https://gist.github.com/reversepanda/5814547
		







