class DotParticles
	_system: null
	_dot: null

	numParticles: 0
	particleArea: 1000

	_texture: null

	constructor: (dot, numParticles = null) ->

		@_dot = dot

		#@_texture = THREE.ImageUtils.loadTexture('assets/images/snowflake_.png')

		if !numParticles
			@numParticles = Math.round(relax.math.lerp(Math.random(), 5, 10))
		else
			@numParticles = numParticles

		@createSystem()


	createSystem: () ->
		@_dot.remove @_system if @_system
		@particles = []

		particles = new THREE.Geometry()

		pMaterial = new THREE.ParticleBasicMaterial
			color: @_dot.getColor()
			#map: @_texture
			transparent: true
			size: 5

		#now create the individual particles
		for i in [0..@numParticles] by 1
			# create a particle with random
			# position values, -250 -> 250
			pX = Math.round(relax.math.lerp(Math.random(), @particleArea * -0.5, @particleArea * 0.5))
			pY = Math.round(relax.math.lerp(Math.random(), @particleArea * -0.5, @particleArea * 0.5))
			pZ = Math.round(relax.math.lerp(Math.random(), @particleArea * -0.5, @particleArea * 0.5))

			particle = new THREE.Vector3(pX, pY, pZ)

			axis = new THREE.Vector3( 0, 1, 0 )
			angle = Math.random() * (Math.PI * 2)
			matrix = new THREE.Matrix4().makeRotationAxis( axis, angle )

			particle.applyMatrix4(matrix)

			# add it to the geometry
			particles.vertices.push(particle)

		# create the particle system
		@_system = new THREE.ParticleSystem(particles, pMaterial)

		# add it to the container
		@_dot.add @_system

	updateColorTo: (color) -> @_system.material.color = color

	update: ->

		# v = @_system.geometry.vertices
		# for vert in v
		# 	if !vert.speed
		# 		vert.speed = Math.random() * 0.02
		# 		vert.area = Math.random() * 4
				
		# 	vert.x += Math.sin(vert.speed)
		# 	vert.y += Math.cos(vert.speed)

		# @_system.geometry.verticesNeedUpdate = true
