#/*  ___
#  _/ ..\
# ( \  0/___
#  \    ___)
#  /     \
# /      _\
#`''''``
#author: felix nielsen<felix.nielsen@bacondeczar.com>*/#
class Router extends Backbone.Router

	pages : null#PagesCollection

	_hasRunFirstTime: false #used to check if content is already in the dom.

	constructor: (options) ->
		# define routes here, use global object routes from keystone app
		# bare in mind that backbone is not a fan of starting slash
		options.routes = {} if !options.routes

		options.routes[""] = "homeRoute"
		options.routes[routes.root] = "homeRoute"
		options.routes[@getRouteStr(routes.work)] = "homeRoute"
		options.routes[@getRouteStr(routes.aCase)] = "caseRoute"
		options.routes[@getRouteStr(routes.contact)] = "textRoute"
		options.routes[@getRouteStr(routes.about)] = "textRoute"
		options.routes[@getRouteStr(routes.impressum)] = "textRoute"

		options.routes["filter"] = "filterRoute"
		options.routes["filter/:filter"] = "filterRoute"
		options.routes["*notFound"] = "notFound"

		super(options)

	getRouteStr: (str) -> if str.substr(0, 1) is "/" then str.substr(1, str.length) else str

	initialize : (options) ->

		#bind getPageContent to the instance scope
		_.bindAll @

		@pages = options.pages

		@on 'route:notFound', (query) -> @routeNotFound()

		@on 'route:homeRoute', (query) -> @doRoute "", "home"
		@on 'route:filterRoute', (query) ->
			# rewrite fragment, so it falls back to the page you want to update
			fragment = "filter"
			@doRoute fragment, "filter"
		@on 'route:caseRoute', (query) -> @doRoute Backbone.history.fragment, "case"
		@on 'route:textRoute', (query) -> @doRoute Backbone.history.fragment, "text"

		return @

	start: ->
		#check for special query string!!
		url = @getURLParameter("page")

		# init history
		Backbone.history.start
			# pushState: if window.location.href.indexOf("localhost") then false else window.history.pushState
			pushState: window.history.pushState
			root: routes.root
			silent: url isnt null #pass silent true if we have a deeplink!

		if url isnt null
			#navigate to url if we have a deeplink.
			@navigate url

	doRoute: (fragment, page = "") ->
		log "** Router : language >", fragment, page

		# page tracking
		window.application.tracking.page Backbone.history.fragment

		pageType = if !page then "not-defined" else page

		@pages.fetch fragment, pageType

	#overriding
	navigate: (fragment, options) ->
		if @ROOT_URL isnt "/"
			u = @ROOT_URL
		else
			u = location.host

		frag = if fragment.split(u)[1] is undefined then fragment else fragment.split(u)[1]

		#call super
		super frag, 
			trigger: true

	navigateToParentPage: ->
		# parent = /electronic/products/product1 = /electronic/products
		str = Backbone.history.fragment
		lastSlash = str.lastIndexOf "/"
		parentURL = str.substring 0, lastSlash+1
		@navigate parentURL

	routeNotFound : ->
		console.warn "route not found!", Backbone.history.fragment
		#@navigate("/") #bad practice.. route to a 404 instead.

	#getURLParameter('page')
	getURLParameter: (name) -> 
		reg = new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)')
		search = (reg.exec(window.location.search) or [null, ""])
		url = decodeURIComponent(search[1].replace(/\+/g, "%20")) or null

		if url is null
			return null
		else
			return url



