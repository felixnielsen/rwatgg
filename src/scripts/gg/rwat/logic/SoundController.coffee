class SoundController
	_mainLoop: null
	_caseLoop: null
	_textLoop: null

	_sounds: null

	constructor: ->
		@_mainLoop = new buzz.sound "/sound/Drone Loop (Main)",
			formats: [ "ogg", "mp3" ]

		@_mainLoop.bind "canplaythrough", =>
			# create these guys, but don't play them yet.
			@_caseLoop = new buzz.sound "/sound/Drone Loop (Case)",
				formats: [ "ogg", "mp3" ]

			@_textLoop = new buzz.sound "/sound/Drone Loop (About)",
				formats: [ "ogg", "mp3" ]

		@_mainLoop.play().fadeIn(3000).loop()

		window.application.pages.on "updated", @onPageUpdated, @

		@_sounds = []

		@_sounds["hover"] = []
		@_sounds["hover_index"] = -1
		for i in [1..8] by 1
			@_sounds["hover"].push new buzz.sound "/sound/Bit "+i,
				formats: [ "ogg", "mp3" ]

		@_sounds["click"] = []
		@_sounds["click_index"] = -1
		for i in [1..6] by 1
			@_sounds["click"].push new buzz.sound "/sound/Click "+i,
				formats: [ "ogg", "mp3" ]

	onPageUpdated: (model, dto) ->
		if dto.type is "text"
			@_caseLoop?.stop()
			if @_textLoop
				@_textLoop.setPercent(0).play().fadeIn(3000).loop()
				@_mainLoop.fadeOut 3000
		else if dto.type is "case"
			@_textLoop?.stop()
			if @_caseLoop
				@_caseLoop.setPercent(0).play().fadeIn(3000).loop()
				@_mainLoop.fadeOut 3000
		else
			@_caseLoop?.fadeOut 3000, ->
				@stop() #TODO, only fadeOut if playing?
			@_textLoop?.fadeOut 3000, ->
				@stop() #TODO, only fadeOut if playing?

			@_mainLoop.fadeIn 3000

		if window.location.href.indexOf("localhost") isnt -1
			for sound in buzz.sounds
				sound.mute()

	play: (category = null) ->
		if category and @_sounds[category]
			num = @_sounds[category+"_index"]
			while @_sounds[category+"_index"] is num
				num = Math.floor(Math.random() * @_sounds[category].length)

			@_sounds[category+"_index"] = num
			@_sounds[category][@_sounds[category+"_index"]].setPercent(0).play()

