class PageModel extends Backbone.Model

	@PAGES: null
	
	defaults:
		active: false

	initialize: ->
		_.bindAll @

	navigateTo: -> window.application.router.navigate @get("url")

	setActiveTo: (state, newModel = null) ->
		@set "active", state

		if @get "active"

			log @get("type")

			type = if !PageModel.PAGES[@get("type")] then DefaultPage else PageModel.PAGES[@get("type")]

			@set "view", new type
				model: @

			#add page type
			@get("view").$el.addClass @get("type")

			#load data here.. and call onModelReadyAfter that.
			@onModelReady()
		else
			@removeView(newModel)

	onModelReady: ->
		view = @get("view")
		view.animateIn()

		view.render()
		@get("context").prepend view.el

		@trigger "ready"

	removeView: (newModel = null) ->
		if @get("view")
			@get("view").animateOut newModel
			@unset "view"

		#animateOut?
		@unbind()
		@remove()

	remove: ->
		#...

	# example object to be passed to setPageTypes
	# obj =
	# 	"home": HomePage
	# 	"dictionary": DictionaryPage
	# 	"guide": GuidePage
	# 	"guide-detail": GuideDetailPage
	# 	"blog": DefaultPage
	@setPageTypes: (obj) ->
		PageModel.PAGES = obj
