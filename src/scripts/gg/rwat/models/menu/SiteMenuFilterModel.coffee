class SiteMenuFilterModel extends Backbone.Model
	
	defaults:
		"visible": false
		"button-visible": true
		"filters": []

	initialize: ->
		window.application.pages.on "updated", @onPageUpdated, @

		@set "view", new SiteMenuFilterView
			el: "#filter-list"
			model: @

		@set "button", new SiteMenuFilterButton
			el: "#filter-button"
			model: @

	toggleFilter: (filter) ->
		filters = @get "filters"

		addToCollection = true

		if filters.indexOf(filter) is -1
			filters.push filter
		else
			addToCollection = false
			filters.splice(filters.indexOf(filter), 1)

		if filters.length is 0
			window.application.router.navigate "filter"
		else
			window.application.router.navigate "filter/"+filters.toString()

		return addToCollection


	onPageUpdated: (model, dto) ->

		log "onPageUpdated > model"

		url = dto.url
		isFilterURL = url.indexOf("filter") isnt -1

		if dto and dto.fragment
			filters = dto.fragment.split("filter/")
			if filters[1]
				filters = filters[1].split(",")

				@set "filters", filters

		@set "visible", isFilterURL
		@get("view").update(@get("filters"))

