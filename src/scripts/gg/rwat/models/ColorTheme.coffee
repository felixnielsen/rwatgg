class ColorTheme extends Backbone.Model
	defaults:
		"primary": "black"
		"secondary": "white"

	initialize: ->
		window.application.pages.on "updated", @onPageUpdated, @

	onPageUpdated: (model, dto) ->
		$("#color-background").removeClass "color-" + @get "name" if @get "name"
		$("body").removeClass "color-" + @get "name" if @get "name"

		if dto.type is "text"
			#white
			@set
				"name": "white"
				"primary": "white"
				"secondary": "black"
		else
			#black
			@set
				"name": "black"
				"primary": "black"
				"secondary": "white"

		$("#color-background").addClass "color-" + @get "name"
		$("body").addClass "color-" + @get "name"

		


