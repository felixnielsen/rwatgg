var keystone = require('keystone'),
	Types = keystone.Field.Types;

var CasesList = new keystone.List('Cases', {
	autokey: { from: 'name', path: 'key' }
});


CasesList.add({
	name: { type: String, required: true },
    type: { type: Types.Relationship, ref: 'CaseType', many: true },
    content: { type: Types.Relationship, ref: 'CaseContent', many: true },
    color: { type: Types.Text },
    published: { type: Types.Boolean, default: false  },
    offline: { type: Types.Boolean, default: false  }
});

CasesList.addPattern('standard meta');
CasesList.register();
