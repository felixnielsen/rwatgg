var keystone = require('keystone'),
	Types = keystone.Field.Types;

var SocialLinks = new keystone.List('SocialLinks', {
	autokey: { from: 'name', path: 'key' },
	nodelete: true,
	nocreate: true
});


SocialLinks.add({
	name: { type: String, required: true },
	link: { type: Types.Url, height: 10 }
});


SocialLinks.addPattern('standard meta');
SocialLinks.register();


// add the elements, statically
var SL = keystone.list('SocialLinks');
// insert facebook if it is not there.
SL.model
	.find()
	.where("name", "Facebook")
	.exec(function(err, posts) {
    	if(posts.length == 0)
    	{
    		new SL.model({
			    name: 'Facebook',
			    link: 'http://facebook.com/rwatgg'
			}).save(function(err) {
			    // post has been saved	
			});
    	}
    });

// insert twitter if it is not there.
SL.model
	.find()
	.where("name", "Twitter")
	.exec(function(err, posts) {
    	if(posts.length == 0)
    	{
    		new SL.model({
			    name: 'Twitter',
			    link: 'http://twitter.com/rwatgg'
			}).save(function(err) {
			    // post has been saved	
			});
    	}
    });
