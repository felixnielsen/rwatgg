var keystone = require('keystone'),
	Types = keystone.Field.Types;

var TextPages = new keystone.List('TextPages', {
	autokey: { from: 'name', path: 'key' },
	nodelete: true,
	nocreate: true
});


TextPages.add({
	name: { type: String, required: true },
	headline: { type: Types.Text, height: 10 },
    text: {
        left: { type: Types.Html, wysiwyg: true, height: 100 },
        right: { type: Types.Html, wysiwyg: true, height: 100 }
    },
    published: { type: Types.Boolean, default: false  },
    offline: { type: Types.Boolean, default: false  }
});

TextPages.addPattern('standard meta');
TextPages.register();





// add the elements, statically
var TL = keystone.list('TextPages');
// insert facebook if it is not there.
TL.model
	.find()
	.where("name", "About")
	.exec(function(err, posts) {
    	if(posts.length == 0)
    	{
    		new TL.model({
			    name: 'About'
			}).save(function(err) {
			    // post has been saved	
			});
    	}
    });

// insert twitter if it is not there.
TL.model
	.find()
	.where("name", "Contact")
	.exec(function(err, posts) {
    	if(posts.length == 0)
    	{
    		new TL.model({
			    name: 'Contact'
			}).save(function(err) {
			    // post has been saved	
			});
    	}
    });


// insert impressum if it is not there.
TL.model
	.find()
	.where("name", "Impressum")
	.exec(function(err, posts) {
    	if(posts.length == 0)
    	{
    		new TL.model({
			    name: 'Impressum'
			}).save(function(err) {
			    // post has been saved	
			});
    	}
    });
