var keystone = require('keystone'),
    Types = keystone.Field.Types;

var CaseType = new keystone.List('CaseType', {
    autokey: { from: 'name', path: 'key' }
});

CaseType.add({
    name: { type: String, required: true }
});


CaseType.register();
